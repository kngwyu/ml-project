import dataclasses

import numpy as np
from numpy.typing import NDArray

# @dataclasses.dataclass
# class RegressionResult:
#     hessian: NDArray
#     singular_values: NDArray
#     weight: NDArray
#     bias: NDArray


@dataclasses.dataclass(frozen=True)
class RegressionResult:
    weight: float
    bias: float
    loss: float

    def compute_loss(self, body_mass: NDArray, brain_size: NDArray) -> float:
        predicted = body_mass * self.weight + self.bias
        return float(np.mean(0.5 * ((predicted - brain_size) ** 2)))


def regression(body_mass: NDArray, brain_size: NDArray, l2: float) -> RegressionResult:
    n = len(body_mass)
    x = np.hstack((np.ones((n, 1)), body_mass.reshape(-1, 1)))
    gram = x.T @ x  # gram maxtrix
    gram_reg = gram + l2 * np.eye(2)
    gram_reg_inv = np.linalg.inv(gram_reg)
    bias, weight = (gram_reg_inv @ x.T) @ brain_size.reshape(-1, 1)
    predicted = body_mass * weight + bias
    loss = np.sum(0.5 * ((predicted - brain_size) ** 2))
    return RegressionResult(weight=weight, bias=bias, loss=loss)


@dataclasses.dataclass(frozen=True)
class CrossValidationResult:
    results: list[RegressionResult]
    test_errors: list[float]
    train_errors: list[float]


def cross_validation(
    body_mass: NDArray,
    brain_size: NDArray,
    l2: float,
    n: int = 10,
) -> CrossValidationResult:
    k = len(body_mass) // n
    results, test_errors, train_errors = [], [], []
    for i in range(n):
        start = i * k
        end = len(body_mass) if i == n - 1 else (i + 1) * k
        body = np.concatenate((body_mass[:start], body_mass[end:]))
        brain = np.concatenate((brain_size[:start], brain_size[end:]))
        result = regression(body, brain, l2)
        results.append(result)
        test_errors.append(
            result.compute_loss(body_mass[start:end], brain_size[start:end])
        )
        train_errors.append(result.loss / len(body))
    return CrossValidationResult(results, test_errors, train_errors)
