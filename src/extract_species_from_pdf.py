# coding: utf-8
import json
from pathlib import Path

from pdfminer.converter import PDFPageAggregator
from pdfminer.layout import LAParams, LTTextBoxHorizontal
from pdfminer.pdfinterp import PDFPageInterpreter, PDFResourceManager
from pdfminer.pdfpage import PDFPage


def main():
    with Path("/home/yu/Programs/ml-project-code/data/abe2101_data_s2.pdf").open(
        mode="rb"
    ) as f:
        rsrcmgr = PDFResourceManager()
        laparams = LAParams()
        device = PDFPageAggregator(rsrcmgr, laparams=laparams)
        interpreter = PDFPageInterpreter(rsrcmgr, device)
        pages = PDFPage.get_pages(f)
        page = next(pages)
        interpreter.process_page(page)
        layout = device.get_result()

    text_boxes = [obj for obj in layout if isinstance(obj, LTTextBoxHorizontal)]

    species = []
    for text_box in text_boxes:
        text = text_box.get_text()
        if any(map(lambda s: s in text, "abcdefghijklmnopqrstuvwxyz")):
            species.append({"text": text, "pos": (text_box.x0, text_box.y0)})

    species.sort(key=lambda s: -(s["pos"][1] * 1000000 + s["pos"][0]))

    ordered_species = []
    for x in species:
        for line in x["text"].splitlines():
            ordered_species.append(line.replace(" ", "_"))

    with open("../data/species.json", mode="w") as f:
        json.dump(ordered_species, f, indent=2)


if __name__ == "__main__":
    main()
