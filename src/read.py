import dataclasses
import json
from pathlib import Path
from typing import Any

import pandas as pd

_DATA_DIR = Path(__file__).absolute().parent.parent.joinpath("data")
_BRAIN_FILE = _DATA_DIR.joinpath("abe2101_data_s1.txt")
_SPECIES_FILE = _DATA_DIR.joinpath("species.json")
_ORDERS_FILE = _DATA_DIR.joinpath("orders.json")


@dataclasses.dataclass
class GroupRange:
    name: str
    range_: tuple[int, int]

    def __contains__(self, i: int) -> bool:
        a, b = self.range_
        return a <= i and i <= b


@dataclasses.dataclass
class Group:
    range_: GroupRange
    sub_ranges: list[GroupRange]


def read_brain_data(path: Path = _BRAIN_FILE) -> pd.DataFrame:
    return pd.read_table(path.as_posix())


def read_species(path: Path = _SPECIES_FILE) -> list[str]:
    with path.open(mode="r") as f:
        return json.load(f)


def read_orders(path: Path = _ORDERS_FILE) -> dict[str, Any]:
    with path.open(mode="r") as f:
        return json.load(f)


def read_brain_data_with_groups() -> pd.DataFrame:
    df = read_brain_data()
    species = read_species()
    orders = read_orders()
    group_ranges = []
    for name, group in orders.items():
        ai, bi = map(lambda n: species.index(n), (group["start"], group["end"]))
        range_ = GroupRange(name=name, range_=(ai, bi))
        sub_ranges = []
        if "sub" in group:
            for subname, subrange in group["sub"].items():
                subrange_int = tuple(map(lambda n: species.index(n), subrange))
                sub_ranges.append(GroupRange(name=subname, range_=subrange_int))
        group_ranges.append(Group(range_=range_, sub_ranges=sub_ranges))

    groups, sub_groups = [], []
    for name in df["Species"]:
        index = species.index(name)
        group_name, sub_group_name = None, None
        for group in group_ranges:
            if index in group.range_:
                group_name = group.range_.name
                for sub in group.sub_ranges:
                    if index in sub:
                        sub_group_name = sub.name
        groups.append(group_name)
        sub_groups.append(sub_group_name)

    df["Group"] = groups
    df["SubGroup"] = sub_groups
    return df
