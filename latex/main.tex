\documentclass{article}
\usepackage{nips13submit_e,times}
\usepackage{times}
\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
% table
\usepackage{booktabs}

%%%%% Figure utilities
\usepackage{graphicx}
\usepackage{siunitx}
\usepackage{subcaption}
\usepackage[export]{adjustbox}
%%%%% End: Figure Utilities

%%%%%% Math utilities
\usepackage{amsmath}
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{nicefrac}
\usepackage{bm}
\usepackage[mathscr]{euscript}
%%%%%% End: Math utilities

%%%%%% Algorithm utilities
\usepackage{algorithm}
\usepackage{algpseudocode}
%%%%%% End: Algorithm utilities

%%%%% Roman number
\newcounter{num}
\newcommand{\rnum}[1]{\setcounter{num}{#1}(\roman{num})}
%%%%% End: Roman number

%%%%% Bibtex
\usepackage{natbib}
\bibliographystyle{abbrvnat}
\setcitestyle{authoryear,open={(},close={)}}
\def\UrlBreaks{\do\/\do-}
%%%%% End: Bibtex

%%%%% Colors
\usepackage{xcolor}
\definecolor{fg}{cmyk}{0.0, 0.0, 0.02, 0.03}
\definecolor{cyberblue}{HTML}{0016EE}
\definecolor{twilightblue}{HTML}{363b74}
\definecolor{twilightpink}{HTML}{ef4f91}
\definecolor{twilightbluepurple}{HTML}{4d1b7b}
\definecolor{twilightpurple}{HTML}{673888}
%%%%% End: Colors

%%%%% Hyper links
\usepackage{hyperref}
\usepackage{cleveref}
\hypersetup{
  colorlinks=true,
  citecolor=twilightblue,
  linkcolor=twilightpink,
  linktocpage=true,
  urlcolor=twilightpurple,
}
%%%%% End: Hyper links


\title{Foundation of ML course project: \\ How simply can the divergent brain evolution explained?}

\author{Yuji Kanagawa\\ OIST \\ \texttt{yuji.kanagawa@oist.jp} \\}

\nipsfinalcopy

\begin{document}

\maketitle

\begin{abstract}
  The evolution of brain size is a long standing problem in biology and neuroscience, receiving various aspects of interests including learning ability.
  It has been generally believed that the relative brain-size to body-size has stably scaled through evolution.
  Contrary, a recent study by \citet{smaersEvolutionMammalianBrain2021} revealed that there can be divergent paths to larger brain size, by regression analysis based on stochastic process modeling.
  While stochastic processes are powerful tools for modeling branching phenomena, I suspect that \textbf{simpler analysis can imply the same result}.
  In this report, I try to replicate their results in a much simpler way.
  I show that regression results in each phylogenetic groups can have higher test variances than the regression curve fitted on the whole data, suggesting the diversity of relative brain-size across phylogenetic groups.
  The variance of regressed weights in each group also suggests the diverse pass.
\end{abstract}

\section{Data Description and Pre-processing}
I used a brain and body size dataset (\texttt{brain-body-table}) and a phylogenetic tree (\texttt{phylogenetic-tree}) of mammals provided by \citet{smaersEvolutionMammalianBrain2021}.
\texttt{brain-body-table} includes a brain and body mass data for 1418 species, including those who are extinct.
\texttt{phylogenetic-tree} is a phylogenetic tree of these 1418 species, including the all branching information of species estimated in some previous works.
I only used the phylogenetic groups induced by the tree, and did not utilized the tree information.
\texttt{brain-body-table} is used without any pre-processing.
Since \texttt{phylogenetic-tree} is used without any modification.
The original analysis by \citet{smaersEvolutionMammalianBrain2021} is done via phylogenetic regression, which tries to complement ancestral information using stochastic process modeling.
On the other hand, I don't use any information from phylogenetic tree other than groups and sub groups, ignoring the contextual information of species.

\section{Regression Method}
I used a linear regression with L2 regularization to estimate the relative brain size of mammals.
Letting $y$ denote an instance of brain size and $x$ denote body size, I model the brain size by $y = \beta_{\textrm{weight}}x + \beta_{\textrm{bias}}$, where $ \beta_{\textrm{weight}}$ and $\beta_{\textrm{bias}}$ denote the parameter corresponding to weight and bias of this linear model.
Since now the input only has one dimension (i.e., body size), I let the parameter vector $\bm{\beta}$ denote $\bm{\beta} = [\beta_{\textrm{bias}}, \beta_{\textrm{weight}}]$, where $[a,b]$ is making a row vector.
Let $N$ denote the number of input.
I used the minimum squared error (MSE) with L2 regularization as the loss function, which is written by:
$\mathcal{L}(\bm{\beta}) =  \frac{1}{2N} \sum_{i=1}^N (y_i - [1x_i]^T\bm{\beta})^2 + \frac{\lambda}{2N} \sum_{j=1}^2 \bm{\beta}_j^2$, where $\lambda$ denotes the coefficient for regularization.
I used the least square solution $\bm{\beta}^{*} = (\tilde{\bm{X}}^{T}\tilde{\bm{X}} + \lambda \bm{I})^{-1} \tilde{\bm{X}}\bm{y}$ to fit the linear model to the data, where $\tilde{\bm{X}} = \begin{pmatrix}
  1 & x_{1} \\
                                                                                                                                                                                                 \vdots & \vdots \\
                                                                                                                                                                                                 1 & x_{N}
\end{pmatrix}$, $\bm{y} = \begin{pmatrix}y_1 \\ \vdots \\ y_N \end{pmatrix}$, and $\bm{I}$ is two-dimensional identity matrix.

\section{Cross validation and parameter tuning}
\begin{figure}[t]
  \centering
  \includegraphics[width=6cm]{lambda.pdf}
  \caption{Training error and test error}
  \label{figure:lambda}
  \vspace{-2mm}
\end{figure}

Regularization parameter $\lambda$ was determined as the one with the least test error when regressed on the whole species data.
Cross validation was done with the number of k-fold $5$, and $\lambda$ was chosen from $[0.0, 20.0]$.
\Cref{figure:lambda} shows the test and training error with different $\lambda$.
While I don't see significant difference, test error is the minimum around $\lambda=0.01$, so $\lambda=0.01$ was used as a part of linear model throughout this report.

\section{Regression Result}
% Weight variance
% all: 0.006 primate: 0.03 ceta: 0.002 carni: 0.013 chirope: 0.003 rode: 0.0055

\begin{figure}[t]
  \centering
  \includegraphics[width=5.4cm]{reg-all.pdf}
  \includegraphics[width=7.2cm]{reg-orders-all.pdf}
  \caption{
    \textbf{Left:} Regressed line on the whole species.
    \textbf{Right:} Regressed lines on each phylogenetic group, shown in one figure.
  }
  \label{figure:reg-orders-all}
  \vspace{-2mm}
\end{figure}

First, I trained the linear regression model on the whole species, as shown in the left figure of \cref{figure:reg-orders-all}.
Multiple lines show the regressed line on a data subset prepared for cross validation, slightly indicating the variance of this model.
We can see that the model fits the data well, implying that the linear relationship between body and brain size is clear.

\begin{figure}[t]
  \centering
  \includegraphics[width=10cm]{reg-orders-each.pdf}
  \caption{Regressed lines on each phylogenetic group, shown with each group.}
  \label{figure:reg-orders-each}
  \vspace{-2mm}
\end{figure}

\input{table}

Then, I trained the model on each phylogenetic group. I used nine groups, and the grouping is almost based on the order of species. \Cref{figure:reg-orders-each} shows the regression result on each group.
The right figure of \cref{figure:reg-orders-all} shows how differently learned models behave depending on the group used for training.
We can see a slight, but clear difference between each model.
As also shown in \cref{table:reg}, the slope of primates is the most steepest, and the one of Lagomorpha is the mildest.

Moreover, \Cref{table:reg} shows the error of models on the whole species, test errors measured by cross validation within each group, and variances of these errors.
Notably, some groups have larger variances of test errors, which mean that sometimes each groups can include more diverse data points and is more difficult to learn by linear models.
This observation suggests that species could have diverse relative brain size even within groups of phylogenetic ally close species, which backs up the divergent pass hypothesis shown by \citet{smaersEvolutionMammalianBrain2021}.
In such a group that contains diverse relative brain size, the relationship between the group and the relative brain size is weak.
However, we can see that there is a significant variance of total errors between species, suggesting that the correlation between the group an brain size is sometimes strong.

\begin{figure}[t]
  \centering
  \includegraphics[width=10cm]{primate.pdf}
  \includegraphics[width=10cm]{cetartiodactyla.pdf}
  \caption{
    Regressed lines on each phylogenetic sub groups in a few groups.
    \textbf{Top:} primates.
    \textbf{Bottom:} cetartiodactyla.
  }
  \vspace{-2mm}
  \label{figure:reg-sub}
\end{figure}

\begin{table}
\centering
\caption{Variance of regressed weight for each group that has sub groups more than 3.}
\label{table:var}
\begin{tabular}{lr}
\toprule
  Group &  Weight variance\\
\midrule
  Primates & 0.03 \\
  Cetartiodactyla & 0.002 \\
  Carnivora & 0.013 \\
  Chiroptera & 0.003 \\
  Rodenia & 0.0055 \\
  All & 0.006 \\
\bottomrule
\end{tabular}
\end{table}

I also conducted regression analysis on each subgroup of a few large groups that have more than three sub groups.
\Cref{figure:reg-sub} shows examples of primates (e.g., humans) and cetartiodactyla (e.g., whales).
\Cref{table:var} shows the variance of weights regressed on each sub group.
The result shows that primates and carnivora have relatively large weight variances, suggesting the diversity of relative brain size.

\section{Conclusion}
By carefully treating variance of test errors and weights of simple regression models, I analyzed the diversity of relative brain size of mammals. This result backs up \citep{smaersEvolutionMammalianBrain2021} with much simpler analysis.

\bibliography{reference}

\end{document}
