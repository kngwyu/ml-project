$pdflatex = 'pdflatex %O -synctex=1 -file-line-error -halt-on-error -interaction=nonstopmode %S';
$biber = 'biber %O --bblencoding=utf8 -u -U --output_safechars %B';
$pdf_mode = 3;
